import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers:[BnbServiceService]
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();
  ErrorphoneNumber:boolean=false;
  forgotPwd:boolean=true;
  verifyPwd:boolean=false;
  phone:string;

  ngOnInit() {
  }

  validateMobile(event){
  	var phoneno = /^[789]\d{9}$/; 
	var numberOnly = /^[0-9]*(?:\.\d{1,2})?$/; 
	if ( !event.target.value || !event.target.value.match(phoneno) || !event.target.value.match(numberOnly)) {
	      this.ErrorphoneNumber=true;
	    }else{
	    	this.ErrorphoneNumber=false;
	    }
  }


  msg:string;
  error:boolean=false;
  forgotPassword(value){
    this.loading = true;
    this.phone=value.phoneNumber;
  	console.log(value);
  	this.http.post(this.ref.baseURL()+'hospitals/forgot-password',
               { "phoneNumber":value.phoneNumber })
                  .map(response => response.json())
                  .subscribe((result) => {
                  	console.log(result);
                    if(result.success==false){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                    }else if(result.success==true){
                      this.loading = false;
                      this.forgotPwd=false;
                      this.verifyPwd=true;
                    }
                  });
  }

  verifyPassword(value){
    this.loading = true;
  	console.log(value);
  	this.http.post(this.ref.baseURL()+'hospitals/forgot-password/verify',
               {
				 "phoneNumber":this.phone,
				 "verificationCode":value.OTP,
				 "newPassword":value.newPassword
				})
                  .map(response => response.json())
                  .subscribe((result) => {
                  	console.log(result);
                    if(result.success==false){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                    }else if(result.success==true){
                      this.loading = false;
                      this.router.navigate(['login']);
                    }
                  });
  }
  

}
