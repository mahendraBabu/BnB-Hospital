import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[BnbServiceService]
})
export class LoginComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();
  ngOnInit() {
  }

  msg:string;
  error:boolean=false;
  loginSubmit(value){
    this.loading = true;
  	console.log(value);
  	this.http.post(this.ref.baseURL()+'hospitals/login',
               {
			          username:value.username, 
			          password:value.password,
			          "appVersion": "1",
			          "platform": "an",
			          "deviceInfo": {
			              "deviceId": "abcd",
			              "IMEINumber": "defg",
			              "registrationId":"1"
			          }
			      }
               )
                  .map(response => response.json())
                  .subscribe((result) => {
                  	console.log(result);
                    if(result.success==false){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                    }else if(result.success==true){
                      localStorage.setItem('token', 'Bearer '+result.data.token);
                      localStorage.setItem('hospitalId', result.data.hospitalId);
                      
                      this.isApprovedByAdmin();
                    }
                  });
  }
  
  isApprovedByAdmin(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/hospitalDetails';
    this.http.get(url,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
                localStorage.setItem('approved', res.data["0"].isApprovedByAdmin);
                if (res.data["0"].isApprovedByAdmin==true) {
                        this.router.navigate(['dashboard']);
                        this.loading = false;
                      }else{
                        this.router.navigate(['notAtcivated']);
                        this.loading = false;
                      }
      //this.loading = false;
    });
  
  }
}
