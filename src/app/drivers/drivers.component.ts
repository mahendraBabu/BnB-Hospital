import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';


@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.css'],
  providers:[BnbServiceService]
})
export class DriversComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  drivers:driver[];
  apiRoot: string = this.ref.baseURL();
  public loading = true;

  ngOnInit() {
    this.loading = true;
  	this.driverList();
  }
  checkForIsDeletedDrivers(obj){
     return (obj.isDeleted===false);
   }
  driverList(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/drivers/driverList';
    this.http.get(url,opts).map(response => response.json()).subscribe((drivers)=>{
      console.log(drivers);
      /*var driverResults = drivers.data.filter(this.checkForIsDeletedDrivers);*/
      this.drivers = drivers.data;  
      this.loading = false;
    });
  
  }

  deleteDriver(driverId, e){
    this.loading = true;
    let status;
    if (e.target.checked) {
      status=false;
    }else {
      status= true;
    }
    let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'hospitals/drivers/deleteDriver';
      let Details=JSON.stringify({ "isDeleted": status,
                                       "driverId":driverId});
    
      this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.driverList();
      this.loading = false;
    });
  }

  /*deleteDriver(driverId){
   
    let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'hospitals/drivers/deleteDriver';
      let Details=JSON.stringify({  "isDeleted":"true",
                                     "driverId":driverId});
    
      this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.driverList();
    });
  }*/

}

export interface driver{
  _id:string,
  username:string,
  phoneNumber:string,
  employeeId:string,
  driverStatus:string,
  driverCategory:string

}