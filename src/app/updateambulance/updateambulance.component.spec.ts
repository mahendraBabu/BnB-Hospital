import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateambulanceComponent } from './updateambulance.component';

describe('UpdateambulanceComponent', () => {
  let component: UpdateambulanceComponent;
  let fixture: ComponentFixture<UpdateambulanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateambulanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateambulanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
