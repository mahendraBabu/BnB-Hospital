import { TestBed, async, inject } from '@angular/core/testing';

import { NotActivatedGuard } from './not-activated.guard';

describe('NotActivatedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotActivatedGuard]
    });
  });

  it('should ...', inject([NotActivatedGuard], (guard: NotActivatedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
