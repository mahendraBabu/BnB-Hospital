
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

import { Component, OnInit ,ElementRef, NgZone,ViewChild} from '@angular/core';
import { FormControl , FormsModule} from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[BnbServiceService]
})

export class RegisterComponent implements OnInit {
  @ViewChild('myInput') myInputVariable: any;

  public loading:boolean;
  public latitude: any;
  public longitude: any;
  public searchControl: FormControl;
  public zoom: number;
  result:any; 
  msg:any;

    days:any = ['sunday','monday'];
   time : any = ['10','12'];
   services:any= ['pickup','drop'];
   loc:any=[];
   errmsg:string;
   lat:any;
   lng:any;

   completeAddr:any='';
   ErrorhospitalName:boolean=false;
   Errorusername:boolean=false;
    Errorpassword:boolean=false;
    ErrorphoneNumber:boolean=false;
    ErrorcompleteAddr:boolean=false;
    Erroremail:boolean=false;
    ErrorcertificateNo:boolean=false;
    Errorcategory:boolean=false;
    Erroraddress:boolean=false;
    fileError:boolean=false;
    ErrorHospphoneNumber:boolean=false;
    formFlag:boolean=false;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(private ref:BnbServiceService, private http:Http, private mapsAPILoader: MapsAPILoader,private ngZone: NgZone, private router:Router) { }
  public formData:FormData;
  file3:File;
  file:any;
  fileUpload(event){
    this.formData =  new FormData();
    this.fileError=false;
    /*this.formData.delete('file');*/
    const imageRegExt=/([a-zA-Z0-9\s_\\.\-:() ])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/;
    this.file3 = event.target.files[0];
    if (!this.file3.name.match(imageRegExt)) {
         this.formFlag=false;
         this.fileError=true;
      }
      
      this.formData.append("file", this.file3, this.file3.name);
      

      if (this.file3.name.match(imageRegExt)) {
         this.formFlag=true;
      }
     
      console.log(this.formData);
    }
  ngOnInit() {
    //set google maps defaults
    this.zoom = 8;
    this.latitude=12.95396;
    this.longitude =77.4908519;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        //types: ["address"]

      });


      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.lat=place.geometry.location.lat();
          this.lng=place.geometry.location.lng();
          this.zoom = 17;
          console.log(place.name+", "+place.formatted_address);
          this.completeAddr=place.name+", "+place.formatted_address;
        });
      });
    });
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  //this.http.get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+place+"&components=country:in&key="+key).map(res => res.json()).subscribe(result => this.result = result);
  successFn(){
    this.router.navigate(['login']);
  }

  submit(value){
   console.log(value);
   
    this.Errorusername=false;
    this.Errorpassword=false;
    this.ErrorhospitalName=false;
    this.ErrorphoneNumber=false;
    this.Erroremail=false;
    this.ErrorcertificateNo=false;
    this.Errorcategory=false;
    this.fileError=false;
    this.Erroraddress=false;
    this.ErrorHospphoneNumber=false;
     var phoneno = /^[789]\d{9}$/; 

     var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     var numberOnly = /^[0-9]*(?:\.\d{1,2})?$/; 
    let status:boolean =true;
   /* if (!this.formData.get('file')) {
      this.fileError=true;
      status=false;
    } */ 
    if (this.formFlag==false) {
      this.fileError=true;
      status=false;
    }
   if ( !value.hospitalName) {
      this.ErrorhospitalName=true;
      status=false;
    } if ( !value.username) {
      this.Errorusername=true;
      status=false;
    }  if ( !value.password) {
      this.Errorpassword=true;
      status=false;
    }  if ( !value.phoneNumber || !value.phoneNumber.match(phoneno) || !value.phoneNumber.match(numberOnly)) {
      this.ErrorphoneNumber=true;
      status=false;
    } 
  
     if ( this.completeAddr=='') {
      this.Erroraddress=true;
      status=false;
    }  if ( !value.email || !value.email.match(emailRegExp)) {
      this.Erroremail=true;
      status=false;
    }  if ( !value.certificateNo) {
      this.ErrorcertificateNo=true;
      status=false;
    }  if ( !value.category) {
      this.Errorcategory=true;
      status=false;
    }
    
 
    if ( !value.HospphoneNumber || !value.HospphoneNumber.match(numberOnly) ) {
      this.ErrorHospphoneNumber=true;
      status=false;
    }

   
    if(status==true) {
      this.loc.push(this.lng.toString());
   this.loc.push(this.lat.toString());
     this.loading=true;
   this.formData.append("admin[username]", value.username);
   this.formData.append("admin[password]", value.password);
   this.formData.append("admin[phoneNumber]", value.phoneNumber);
   this.formData.append("admin[appVersion]", "1");
   this.formData.append("admin[platform]", "an");
   this.formData.append("admin[deviceInfo][deviceId]", "abcd");
   this.formData.append("admin[deviceInfo][IMEINumber]", "defg");
   this.formData.append("hospital[name]", value.hospitalName);
   this.formData.append("hospital[address]", this.completeAddr);
   this.formData.append("hospital[location]", JSON.stringify(this.loc));
   this.formData.append("hospital[workingDays]", JSON.stringify(this.days) );
   this.formData.append("hospital[workingTime]",  JSON.stringify(this.time));
   this.formData.append("hospital[specialization]", "Orthopedic");
   this.formData.append("hospital[emailId]", value.email);
   this.formData.append("hospital[establishedCertificateNumber]", value.certificateNo);
   this.formData.append("hospital[servicesOffered]",  JSON.stringify(this.services));
   this.formData.append("hospital[category]", value.category);
   this.formData.append("hospital[phoneNumber]", value.HospphoneNumber);
   
   /*let headers: Headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    let opts: RequestOptions = new RequestOptions();
    opts.headers = headers;*/

    console.log(this.loc)
    this.http.post(this.ref.baseURL()+'hospitals/signup', this.formData)
    .map(res => res.json())
    .subscribe(
               (res) => {
                
                 if (res.success==true) {
                                  this.loading = false;
                                  this.errmsg = "Registered Succesfully.";
                                  this.loginSubmit(value.username,value.password);
                                  //setTimeout(function(){ location.reload() }, 2000);

                              }else if (res.success==false) {
                                this.errmsg = res.message;
                                this.formData =  new FormData();
                                this.formFlag=false;
                                //this.file='';
                                console.log(this.myInputVariable.nativeElement.files);
                                this.myInputVariable.nativeElement.value = "";
                                console.log(this.myInputVariable.nativeElement.files);
                                /*this.formData.delete('admin[username]');
                                this.formData.delete('admin[password]');
                                this.formData.delete('admin[phoneNumber]');
                                this.formData.delete('admin[appVersion]');
                                this.formData.delete('admin[platform]');
                                this.formData.delete('admin[deviceInfo][deviceId]');
                                this.formData.delete('admin[deviceInfo][IMEINumber]');
                                this.formData.delete('hospital[name]');
                                this.formData.delete('hospital[address]');
                                this.formData.delete('hospital[location]');
                                this.formData.delete('hospital[workingDays]');
                                this.formData.delete('hospital[workingTime]');
                                this.formData.delete('hospital[specialization]');
                                this.formData.delete('hospital[emailId]');
                                this.formData.delete('hospital[establishedCertificateNumber]');
                                this.formData.delete('hospital[servicesOffered]');
                                this.formData.delete('hospital[category]');
                                this.formData.delete('hospital[phoneNumber]');
                                this.formData.delete('file');*/
                                this.loc.pop();
                                this.loc.pop();
                                console.log(this.loc);  
                                this.fileError=false;
                                this.loading = false;
                              }else{
                                  this.loading = false;
                                 this.errmsg = "Oops ! Something went wrong..( Server Error)"; 
                               }
               });
     }
  }


  error:boolean=false;
  loginSubmit(username,password){
    this.loading = true;
   
    this.http.post(this.ref.baseURL()+'hospitals/login',
               {
                username:username, 
                password:password,
                "appVersion": "1",
                "platform": "an",
                "deviceInfo": {
                    "deviceId": "abcd",
                    "IMEINumber": "defg",
                    "registrationId":"1"
                }
            }
               )
                  .map(response => response.json())
                  .subscribe((result) => {
                    console.log(result);
                    if(result.success==false){
                      this.error=true;
                      this.msg=result.message;
                      this.loading = false;
                    }else if(result.success==true){
                      localStorage.setItem('token', 'Bearer '+result.data.token);
                      localStorage.setItem('hospitalId', result.data.hospitalId);
                      
                      this.isApprovedByAdmin();
                    }
                  });
  }
  
  isApprovedByAdmin(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.ref.baseURL()+'hospitals/hospitalDetails';
    this.http.get(url,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      localStorage.setItem('approved', res.data["0"].isApprovedByAdmin);
                if (res.data["0"].isApprovedByAdmin==true) {
                        this.router.navigate(['dashboard']);
                        this.loading = false;
                      }else{
                        this.router.navigate(['notAtcivated']);
                        this.loading = false;
                      }
      //this.loading = false;
    });
  
  }
  
}
