import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.css'],
  providers:[BnbServiceService]
})
export class AddDriverComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  apiRoot: string = this.ref.baseURL();
  errmsg:string;
  public loading = false;
  ErrorphoneNumber:boolean=false;
  
  ngOnInit() {
  }

  addDriverSubmit(value){
    let status:boolean =true;
    this.loading = true;
    var phoneno = /^[789]\d{9}$/; 
    if ( !value.phoneNumber || !value.phoneNumber.match(phoneno)) {
          this.ErrorphoneNumber=true;
          status=false;
        } 
    if(status==true) {
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    let driver=JSON.stringify({
								 "username":value.username,
								 "password":value.password,
								 "phoneNumber":value.phoneNumber,
								 "driverCategory":value.driverCategory,
								 "employeeId":value.employeeId,
								 "driverLicenseNumber":value.driverLicenseNumber 
								});
    let url:string = this.apiRoot+'hospitals/drivers';
    this.http.post(url,driver,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      if (res.success==true) {
        	this.loading = false;
        	this.errmsg = "Driver Added Succesfully.";
          setTimeout(function(){ location.reload() }, 2000);

      }else if (res.success==false) {
        this.loading = false;
        this.errmsg = res.message;
      }else{
          this.loading = false;
         this.errmsg = "Oops ! Something went wrong..( Server Error)"; 
       }
    });

  }
   }
}
