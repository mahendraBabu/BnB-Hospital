
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

import { Component, OnInit ,ElementRef, NgZone,ViewChild} from '@angular/core';
import { FormControl , FormsModule} from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers:[BnbServiceService]
})
export class ProfileComponent implements OnInit {

    constructor(private ref:BnbServiceService, private http:Http, private mapsAPILoader: MapsAPILoader,private ngZone: NgZone, private router:Router) { }

  public loading:boolean;
  public latitude: any;
  public longitude: any;
  public searchControl: FormControl;
  public zoom: number;
  apiRoot: string = this.ref.baseURL();

  username:string;
  address:string;
  category:string;
  name:string;
  phoneNumber:string;
  emailId:string;
  servicesOffered:any[];
  services:any= ['pickup','drop'];
   loc:any=[];
   errmsg:string;
   lat:any;
   lng:any;
   completeAddr:any='';

   @ViewChild("search")
  public searchElementRef: ElementRef;


  ngOnInit() {
  	this.hospitalDetails();
    this.initMap();
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }


  initMap(){
    //set google maps defaults
    this.zoom = 16;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        //types: ["address"]

      });


      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.lat=place.geometry.location.lat();
          this.lng=place.geometry.location.lng();
          this.zoom = 17;
          console.log(place.name+", "+place.formatted_address);
          this.completeAddr=place.name+", "+place.formatted_address;
        });
      });
    });
  }
  submit(value){
    location.reload();

  }

  updateProfile(value){
    console.log(value);
      this.loading = true;
      /*this.loc.push(this.lng.toString());
       this.loc.push(this.lat.toString());*/
      let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let driver=JSON.stringify({
                    "name":value.name,
                   "address":this.completeAddr,
                   "location":[this.lng,this.lat],
                   "phoneNumber":value.phoneNumber,
                   "emailId":value.emailId,
                   "category":value.category,
                   "hospitalId":localStorage.getItem('hospitalId'),
                   "specialization":"Ortho arth",
                    "servicesOffered":["pickup"],
                  });
      console.log('driver',driver)
      let url:string = this.apiRoot+'hospitals/updateHospitalDetails';
      this.http.post(url,driver,opts).map(response => response.json()).subscribe((res)=>{
        console.log(res);
        if (res.success==true) {
            this.loading = false;
            
            this.errmsg = "Updated Succesfully.";

        }else if (res.success==false) {
          this.loading = false;
          this.errmsg = res.message;
        }else{
            this.loading = false;
           this.errmsg = "Oops ! Something went wrong..( Server Error)"; 
         }
      });
   }

  hospitalDetails(){
    this.loading=true;
    let token:string = localStorage.getItem('token');
    let hospitalId:string = localStorage.getItem('hospitalId');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    headers.append('hospitalId', hospitalId);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/hospitalDetails';
    this.http.get(url,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.name=res.data["0"].name;
      this.username=res.data["0"].username;
      this.completeAddr=res.data["0"].address;
      this.category=res.data["0"].category;
      this.phoneNumber=res.data["0"].phoneNumber;
      this.servicesOffered=res.data["0"].servicesOffered;
      this.emailId=res.data["0"].emailId;
      this.lng=res.data["0"].location.coordinates["0"];
      this.lat=res.data["0"].location.coordinates["1"];
      this.longitude=res.data["0"].location.coordinates["0"];
      this.latitude=res.data["0"].location.coordinates["1"];
                /*if (res.data["0"].isApprovedByAdmin==true) {
                        this.router.navigate(['dashboard']);
                        this.loading = false;
                      }else{
                        this.router.navigate(['notAtcivated']);
                        this.loading = false;
                      }*/
      this.loading = false;
    });
  
  }

}
