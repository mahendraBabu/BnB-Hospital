import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';
import { LoadingModule } from 'ngx-loading';
import { UiSwitchModule } from 'ngx-toggle-switch/src';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { DriversComponent } from './drivers/drivers.component';
import { AmbulanceComponent } from './ambulance/ambulance.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { AddAmbulanceComponent } from './add-ambulance/add-ambulance.component';
import { ProfileComponent } from './profile/profile.component';
import { LogComponent } from './log/log.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';
import { NotActivatedComponent } from './not-activated/not-activated.component';
import {AuthGuard} from './auth.guard';
import { UpdatedriverComponent } from './updatedriver/updatedriver.component';
import { UpdateambulanceComponent } from './updateambulance/updateambulance.component';
import { TrackComponent } from './track/track.component';
import { MomentModule } from 'angular2-moment';
import {NgxPaginationModule} from 'ngx-pagination';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    MenuComponent,
    DriversComponent,
    AmbulanceComponent,
    AddDriverComponent,
    AddAmbulanceComponent,
    ProfileComponent,
    LogComponent,
    TripDetailsComponent,
    NotActivatedComponent,
    UpdatedriverComponent,
    UpdateambulanceComponent,
    TrackComponent,
    ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    LoadingModule.forRoot({
        backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
        backdropBorderRadius: '4px',
        primaryColour: '#00BCD4', 
        secondaryColour: '#00BCD4', 
        tertiaryColour: '#00BCD4'
    }),
    AgmCoreModule.forRoot({
      //apiKey: "AIzaSyBdth4gx9hvij9hsK_LnTJTxj_bakwCgT8",
      apiKey: "AIzaSyDbQd2KLv-iQOghEsaSiyOSa_bqzaF5WfQ",
      libraries: ["places"]
    }),
    UiSwitchModule,
    MomentModule,
    NgxPaginationModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
