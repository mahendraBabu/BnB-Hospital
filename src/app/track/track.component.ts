import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';
import { MapsAPILoader } from '@agm/core';
import { DatePipe } from '@angular/common';
import { MomentModule } from 'angular2-moment';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css'],
  providers:[BnbServiceService]
})
export class TrackComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router,private route: ActivatedRoute) { }
  apiRoot: string = this.ref.baseURL();
  public loading = false;

  public latitude: number;
  public longitude: number;
  public zoom: number= 8;
  public id:string;

  	createdAt:any;
	name:any;
	phoneNumber:any;
	emergencyType:any;
	tripStatus:any;
	username:any;
	driverPhoneNumber:any;
	userAddress:any;
  vehicleNumber:any;
  mapTrue:boolean=false;
  pay:boolean=false;
   paymentStatus:string;
   totalFare:any; 

  lat: number = 12.95396;
  lng: number = 77.4108519;
   markers: marker[] = [
    {
      lat: 12.95346,
      lng: 77.5908519,
      label: 'U',
      draggable: false
    },
    {
      lat: 12.98396,
      lng: 77.5808519,
      label: 'D',
      draggable: false
    }
  ];

  ngOnInit() {
  	this.id = this.route.snapshot.paramMap.get('id');
  	this.loading = true;
  	this.track(this.id);
    //set current position
    this.setCurrentPosition();
    setInterval(()=>{this.track(this.id) },10 * 1000);
  }
  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  track(id){

  	let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'trip/tripDetails/'+id;
    this.http.get(url,opts).map(response => response.json()).subscribe((trips)=>{
      console.log(trips);
    
      this.createdAt = trips.data.createdAt;
		this.name=trips.data.user.name ; 
		this.phoneNumber=trips.data.user.phoneNumber ; 
		this.emergencyType=trips.data.emergencyType ; 
		this.tripStatus=trips.data.tripStatus ; 
		this.username=trips.data.driver.username ; 
		this.driverPhoneNumber=trips.data.driver.phoneNumber ; 
		this.userAddress=trips.data.userAddress ; 

    
    if(trips.data.tripStatus !== 'assigned'){
      this.markers[1].lat = trips.data.driver.driverCurrentCoordinates[1];
      this.markers[1].lng = trips.data.driver.driverCurrentCoordinates[0];
      this.markers[0].lat = trips.data.location.coordinates[1];
      this.markers[0].lng = trips.data.location.coordinates[0];
      this.mapTrue=true;
    }
    if(trips.data.tripStatus == 'completed'){
      this.totalFare= trips.data.pay.totalFare;
      this.paymentStatus= trips.data.pay.paymentStatus;
      this.pay=true;
    }
      this.vehicleNumber=trips.data.ambulance.vehicleNumber;	
     
      this.loading = false;
    });
  }

}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}