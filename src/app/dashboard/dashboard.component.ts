 import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';
import { MapsAPILoader } from '@agm/core';
import { DatePipe } from '@angular/common';
import { MomentModule } from 'angular2-moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[BnbServiceService]
})
export class DashboardComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  dumb:any;
  leads:lead[];
  acceptedLeads:lead[];
  apiRoot: string = this.ref.baseURL();
  inRequest:boolean=true;
  ifAccepted:boolean=true;
  titleDiv:boolean=true;
  noRidesFound:boolean=false;
  errmsg:string;
  public loading:boolean = false;
  drivers:driver[];
  ambulance:ambulance[];
  selectDriver:boolean=false;
  selectAmbulance:boolean=false;
  selectDriverList:selectDriverList[];
  selctAmbulanceList:selectAmbulanceList[];
  acceptTripDetails:any;
  AcceptTripError:any;
  acceptreqButton:boolean=true;
  //for global trip details
  acceptTripId:string;
  acceptTripdriverId:string;
  acceptTripambulanceId:string;

  successDiv:boolean=false;
  falseDiv:boolean=false;
  acceptedTripAlert:boolean=false;
  yetToBeAssigned:boolean=false;
  yetToBeAssignedTrips:any[];

  hospitalName:string;
  
  ngOnInit() {
    this.loading = true;
    this.checkForNotAssignedTrips();
    this.incomingRequests('pending');
    this.hospitalDetails();
    this.driverList();
    this.ambulanceList();
    
    setInterval(()=>{this.incomingRequests('pending'); },30 * 1000); 
    setInterval(()=>{this.driverList(); }, 30 * 1000); 
    setInterval(()=>{this.ambulanceList(); },30 * 1000); 
     

    
  }

  hospitalDetails(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/hospitalDetails';
    this.http.get(url,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.hospitalName= res.data["0"].name;
        
    });
  
  }

  rejectTrip(tripId){
    this.loading=true;
    let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'trip/rejectTrip';
      let Details=JSON.stringify({ "tripId": tripId});
    
      this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
        
      location.reload();
    });
  }
  checkForNotAssignedTrips(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'trip/tripListing/accepted';
    this.http.get(url,opts).map(response => response.json()).subscribe((trips)=>{
      console.log(trips);
      if (trips.data && trips.data.length) {
        this.acceptedTripAlert=true;
         return true;
      }else{
        this.acceptedTripAlert=false;
        return true;
      }
    });
  }

  showYetToBeAssignedRequests(){
    this.loading=true;
    this.acceptedTripAlert=false;
    this.yetToBeAssigned=true;
    this.inRequest=false;
    this.noRidesFound=false;
    this.titleDiv=false;  

    let trips = this.incomingRequests('accepted');
        
    this.loading=false;
 
  }

  refresh(): void {
      window.location.reload();
  }

  incomingRequests(tripStatus){
    this.loading=true;
    this.noRidesFound=false;
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    let opts: RequestOptions = new RequestOptions();
    opts.headers = headers;
    let url = this.apiRoot+'notification/getActiveRides';

    let urlSearchParams = new URLSearchParams();
    var incomingRequests=[];
    this.http.post(url,
      JSON.stringify({
        hospitalId: localStorage.getItem('hospitalId'),
        tripStatus: tripStatus
      }),opts).map(response => response.json()).subscribe((leadsRes)=>{
    //console.log(leadsRes);

    if (leadsRes.success==true) {
     
      let tempArr:any[];

      /*for (var i = 0, len = leadsRes.data.length; i < len; i++ ) {
        let temp = leadsRes.data[i];
     
        let lat=temp.trip.location.coordinates[1];
        let lang=temp.trip.location.coordinates[0];
        //get address from google places geocode api by sending lat & lang
        this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lang+'&key=AIzaSyDbQd2KLv-iQOghEsaSiyOSa_bqzaF5WfQ').map(response => response.json()).subscribe(data => {
          let reqAddr=data['results'][0]['formatted_address'];
          
          temp.trip.location=reqAddr;
          incomingRequests.push(temp);
          //leadsRes.data[i]=temp;

        });
      }*/
      console.log(leadsRes.data);
      if (tripStatus=='accepted') {
         this.acceptedLeads=leadsRes.data;
         return leadsRes.data;
      }
      
      this.leads=leadsRes.data.reverse();
      this.checkforAvailableResourse();
      this.loading = false;
      return;

    }else if(leadsRes.success==false){
      this.loading = false;
      this.leads=[];
      this.errmsg = leadsRes.message;
      this.noRidesFound=true;
      return;
    }else{
      this.loading = false;
      this.errmsg = "Oops !! Something went wrong.. ( Server Error)";
      return;
    }
    
    
    });
  }
  driverList(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/drivers/driverList';
    this.http.get(url,opts).map(response => response.json()).subscribe((drivers)=>{
      var driverResults = drivers.data.filter(this.checkForIsDeletedDrivers);
      this.drivers = driverResults;
     
    });
  
  }

  ambulanceList(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/ambulance/ambulanceList';
    this.http.get(url,opts).map(response => response.json()).subscribe((ambulances)=>{
      var ambulancesResult = ambulances.data.filter(this.checkForIsDeletedAmbulances);
      this.ambulance = ambulancesResult;
      return ambulances.data;
    });
  
  }

  checkforAvailableResourse(){
    let amb;
    let dri;
    var driverResults;
    var ambResults;
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/ambulance/ambulanceList';
    this.http.get(url,opts).map(response => response.json()).subscribe((ambulances)=>{
      amb=ambulances.data;
    });

    let url2:string = this.apiRoot+'hospitals/drivers/driverList';
    this.http.get(url2,opts).map(response => response.json()).subscribe((drivers)=>{
      
      dri = drivers.data;
       driverResults = dri.filter(this.filterByStatus);
       if ((driverResults & driverResults.length) && (ambResults & ambResults.length)) {
        
         this.acceptreqButton=true;

      }else{
        
        this.acceptreqButton=true;
      }
    });
     
    
  }
assignDriverAmbulance(id){
  this.acceptTripId=id;
  this.driverAmbulanceList(id);
}



  acceptRequest(id){
    
    this.acceptTripId=id;

    this.loading = true;
    this.inRequest=false;
    this.ifAccepted=true;
    this.acceptedTripAlert=false;
    let errmsgAccept:string;
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    let hospId=JSON.stringify({ "tripId": id, "isAccepted":"true"});
    let url:string = this.apiRoot+'trip/accept-trip';
    this.http.post(url,hospId,opts).map(response => response.json()).subscribe((res)=>{
     
      if (res.success==true) {
        this.driverAmbulanceList(id);
      }else if (res.success==false) {
        this.loading = false;
        errmsgAccept = res.message;
      }else{
          this.loading = false;
         errmsgAccept = "Oops ! Something went wrong..( Server Error)"; 
       }
    });
   }
 filterByStatus(obj) {
   return (obj.driverStatus==="isAvailable" && obj.isDeleted===false);
 }
 checkForIsDeletedDrivers(obj){
   return (obj.isDeleted===false);
 }
 checkForIsDeletedAmbulances(obj){
   return (obj.isDeleted===false);
 }
 filterAmbByStatus(obj) {
   return (obj.ambulanceStatus==="isAvailable" && obj.isDeleted===false);
 }
  driverAmbulanceList(id){
      this.yetToBeAssigned=false;
      this.selectDriver=true;
      this.successDiv=false;      
      this.falseDiv=false;
      this.inRequest=false; 
      this.titleDiv=false;  
      this.noRidesFound=false;   
      let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'hospitals/drivers/driverList';
      let activeDrivers;
      this.http.get(url,opts).map(response => response.json()).subscribe((drivers)=>{
       
        let data = drivers.data
        var driverResults = data.filter(this.filterByStatus);
        this.selectDriverList = driverResults;
        this.selectDriver=true;
        this.loading=false;
      });
  }
  reSelectAmbulanceList(){
      this.noRidesFound=false; 
       this.loading=true;
      this.selectAmbulance=true;
      
      this.selectDriver=false;
      let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      let opts = new RequestOptions();
      opts.headers = headers;
      let urlAmbulance:string = this.apiRoot+'hospitals/ambulance/ambulanceList';
    
      let activeAmbulances;
     
      this.http.get(urlAmbulance,opts).map(response => response.json()).subscribe((ambulances)=>{
        let ambuData = ambulances.data
        var ambulanceResults = ambuData.filter(this.filterAmbByStatus);
        this.selctAmbulanceList = ambulanceResults;
        this.loading=false;

      });
      
  }
  selectAmbulanceList(driverId){
    this.noRidesFound=false; 
       this.loading=true;
      this.selectAmbulance=true;
      this.selectDriver=false;
      this.acceptTripdriverId=driverId;
      this.selectDriver=false;
      let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      let opts = new RequestOptions();
      opts.headers = headers;
      let urlAmbulance:string = this.apiRoot+'hospitals/ambulance/ambulanceList';
    
      let activeAmbulances;
     
      this.http.get(urlAmbulance,opts).map(response => response.json()).subscribe((ambulances)=>{
        let ambuData = ambulances.data
        var ambulanceResults = ambuData.filter(this.filterAmbByStatus);
        this.selctAmbulanceList = ambulanceResults;
        this.loading=false;

      });
      
  }

  checkDriver(driverId){
    this.selectAmbulanceList(driverId);
     /*let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'trip/checkAvailabilityStatus';
      let tripDetails=JSON.stringify({ "tripId": this.acceptTripId,
                                        "checkAvailability":'d',
                                       "driverId":driverId});
   
      this.http.post(url,tripDetails,opts).map(response => response.json()).subscribe((res)=>{
      
      if (res.succuss==true) {
        this.selectAmbulanceList(driverId);
      }else if (res.succuss==false) {
       alert(res.message);
       this.driverAmbulanceList(this.acceptTripId);
       this.loading=false;
       

      }else{
        alert("Something went wrong , please try again");
       this.loading=false;
       }
    });*/
  }
  acceptTrip(ambid){
      
      this.loading=true;
      this.noRidesFound=false; 
      this.selectAmbulance=false;
      this.acceptTripambulanceId=ambid;
      let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'trip/assign-driver';
      let tripDetails=JSON.stringify({ "tripId": this.acceptTripId,
                                       "driverId":this.acceptTripdriverId,
                                       "ambulanceId":this.acceptTripambulanceId});
    console.log("assign",tripDetails);
      this.http.post(url,tripDetails,opts).map(response => response.json()).subscribe((res)=>{
      
      if (res.success==true) {
        this.loading = false;
        this.successDiv=true;
        this.driverList();
        this.ambulanceList();
        setTimeout(function(){window.location.href="/"},20000);
      }else if (res.success==false) {
        this.loading = false;
        this.AcceptTripError = res.message; 
        this.falseDiv=true;

      }else{
          this.loading = false;
           this.AcceptTripError = "Oops ! Something went wrong..( Server Error)"; 
           this.falseDiv=true;
       }
    });
  }

  reassignTrip(){
      this.loading=true;
      this.noRidesFound=false; 
      let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'trip/assign-driver';
      let tripDetails=JSON.stringify({ "tripId": this.acceptTripId,
                                       "driverId":this.acceptTripdriverId,
                                       "ambulanceId":this.acceptTripambulanceId});
      console.log("Reassign",tripDetails);
      this.http.post(url,tripDetails,opts).map(response => response.json()).subscribe((res)=>{
      
      if (res.success==true) {
        this.loading = false;
        this.successDiv=true;
        setTimeout(function(){window.location.href="/"},20000);
      }else if (res.success==false) {
        this.loading = false;
        this.AcceptTripError = res.message; 
        this.falseDiv=true;
        
      }else{
          this.loading = false;
           this.AcceptTripError = "Oops ! Something went wrong..( Server Error)"; 
           this.falseDiv=true;
       }
    });
  }

  updateDriverStatus(driverId, e){
    let status;
    if (e.target.checked) {
      status='isAvailable';
    }else {
      status= 'onDuty';
    }
    let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'hospitals/drivers/driverStatus';
      let Details=JSON.stringify({ "driverStatus": status,
                                       "driverId":driverId});
    
      this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.driverList();
    });
  }
    updateAmbulanceStatus(ambulanceId, e){
      this.loading=true;
      let status;
      if (e.target.checked) {
        status='isAvailable';
      }else {
        status= 'onDuty';
      }
      let token:string = localStorage.getItem('token');
        let headers: Headers = new Headers();
        headers.append('Authorization', token);
        headers.append('Content-Type', 'application/json');
        let opts = new RequestOptions();
        opts.headers = headers;
        let url:string = this.apiRoot+'hospitals/ambulance/ambulanceStatus';
        let Details=JSON.stringify({ "ambulanceStatus": status,
                                         "ambulanceId":ambulanceId});
      
        this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
        console.log(res);
        this.ambulanceList();
        this.loading=false;
      });
    }
  /*acceptLead(value){
    console.log(value);
    this.http.post(this.ref.baseURL()+'acceptLead',
               {
                username:value.username, 
                password:value.password,
                "appVersion": "1",
                "platform": "an",
                "deviceInfo": {
                    "deviceId": "abcd",
                    "IMEINumber": "defg",
                    "registrationId":"1"
                }
            }
               )
                  .map(response => response.json())
                  .subscribe((result) => {
                    console.log(result);
                    if(result.success==false){
                      //this.error=true;
                      //this.msg=result.message;
                    }else if(result.success==true){
                      localStorage.setItem('token', 'Bearer '+result.data.token);
                      localStorage.setItem('hospitalId', result.data.hospitalId);
                      this.router.navigate(['dashboard']);
                    }
                  });
  }*/

}

export interface lead{
  _id:string,
  emergencyType:string,
  location:string,
  name:string,
  mobile:string,
  createdAt:any

}

export interface driver{
  _id:string,
  username:string,
  phoneNumber:string,
  employeeId:string,
  driverStatus:string,
  driverCategory:string

}
export interface ambulance{
  _id:string,
  vehicleNumber:string,
  rcNumber:string,
  ambulanceCategory:string,
  imageUrl:string,
  ambulanceStatus:string

}

export interface selectDriverList{
  _id:string,
  username:string,
  phoneNumber:string,
  employeeId:string,
  driverStatus:string,
  driverCategory:string
}

export interface selectAmbulanceList{
  _id:string,
  vehicleNumber:string,
  rcNumber:string,
  ambulanceCategory:string,
  imageUrl:string,
  ambulanceStatus:string
}