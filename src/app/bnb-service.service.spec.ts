import { TestBed, inject } from '@angular/core/testing';

import { BnbServiceService } from './bnb-service.service';

describe('BnbServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BnbServiceService]
    });
  });

  it('should be created', inject([BnbServiceService], (service: BnbServiceService) => {
    expect(service).toBeTruthy();
  }));
});
