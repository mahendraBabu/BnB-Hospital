import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-ambulance',
  templateUrl: './ambulance.component.html',
  styleUrls: ['./ambulance.component.css'],
  providers:[BnbServiceService]
})
export class AmbulanceComponent implements OnInit {
  ambulances:ambulance[];
  public loading:boolean=true;
  editAmbId : string;
  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  apiRoot: string = this.ref.baseURL();

  imgUrl:string='http://139.59.13.184/media/hospitals/'+localStorage.getItem('hospitalId')+'/ambulance/images/';

  ngOnInit() {
  	this.ambulanceList();
    
  }
  ambulanceList(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/ambulance/ambulanceList';
    this.http.get(url,opts).map(response => response.json()).subscribe((ambulances)=>{
      //var ambulancResults = ambulances.data.filter(this.checkForIsDeletedAmbulance);
      this.ambulances = ambulances.data;
      this.loading=false;
    });
  
  }

  checkForIsDeletedAmbulance(obj){
     return (obj.isDeleted===false);
   }

   deleteAmbulance(ambId, e){
    this.loading = true;
    let status;
    if (e.target.checked) {
      status=false;
    }else {
      status= true;
    }
    let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'hospitals/ambulance/deleteAmbulance';
      let Details=JSON.stringify({ "isDeleted": status,
                                       "ambulanceId":ambId});
    
      this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.ambulanceList();
      this.loading = false;
    });
  }

 /* deleteAmbulance(ambId){
   
    let token:string = localStorage.getItem('token');
      let headers: Headers = new Headers();
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      let opts = new RequestOptions();
      opts.headers = headers;
      let url:string = this.apiRoot+'hospitals/ambulance/deleteAmbulance';
      let Details=JSON.stringify({  "isDeleted":"true",
                                     "ambulanceId":ambId});
    
      this.http.post(url,Details,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      this.ambulanceList();
    });
  }*/
}


interface ambulance{
	_id:string,
	vehicleNumber:string,
	rcNumber:string,
	ambulanceCategory:string
	imageUrl:string,
	hospitalId:string

}
