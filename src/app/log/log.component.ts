import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';
import { MapsAPILoader } from '@agm/core';
import { DatePipe } from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';
@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css'],
   providers:[BnbServiceService]
})
export class LogComponent implements OnInit {
  p: number = 1;
  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  apiRoot: string = this.ref.baseURL();
  public loading = false;
  detailsStatus:boolean=false;
  list:boolean=true;
  trips:trip[];
  tripTrack:trip[];
  public latitude: number;
  public longitude: number;
  public zoom: number= 15;
  ngOnInit() {
	//this.loading = true;
	this.filter('all');
	
  }

  filter(value){
  	this.loading = true;
  	let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'trip/tripListing/'+value;
    this.http.get(url,opts).map(response => response.json()).subscribe((trips)=>{
      console.log(trips);
      this.trips = trips.data;
      this.loading = false;
    });
  }

  track(id){
  	this.list=false;
  	this.detailsStatus=true;
  	let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'trip/tripDetails/'+id;
    this.http.get(url,opts).map(response => response.json()).subscribe((trips)=>{
      console.log(trips);
      //this.tripTrack = trips.data;
     
      this.latitude = trips.data.driver.driverCurrentCoordinates[1];
      this.longitude = trips.data.driver.driverCurrentCoordinates[0];
      
      setInterval(()=>{this.track(id); },10000); 
      //this.loading = false;
    });
  }

}

export interface trip{
	_id:string,
	createdAt:string,
	name:string,
	phoneNumber:string,
	emergencyType:string,
	tripStatus:string,
	vehicleNumber:string,
	username:string

}