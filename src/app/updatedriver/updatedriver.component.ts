import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-updatedriver',
  templateUrl: './updatedriver.component.html',
  styleUrls: ['./updatedriver.component.css'],
  providers:[BnbServiceService]
})
export class UpdatedriverComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router,private route: ActivatedRoute) { }
  apiRoot: string = this.ref.baseURL();
  errmsg:string;
  public loading = false;
  drivers:any[];
  username:string;/*
  password:string;*/
  phoneNumber:string;
  driverCategory:string;
  employeeId:string;
  driverLicenseNumber:string;
  public id: string;


  ngOnInit() {
  	this.id = this.route.snapshot.paramMap.get('id');

  	this.driverList(this.id);
  }

  driverList(driverid){
  	this.loading = true;
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    let driver;
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/drivers/driverList';
    this.http.get(url,opts).map(response => response.json()).subscribe((drivers)=>{
      driver = drivers.data.filter(dri => dri._id == driverid);
      //this.drivers = drivers.data;
     //console.log('driver',driver['0'].username);
    
     this.username=driver["0"].username;
     //this.password=driver["0"].password;
     this.phoneNumber=driver["0"].phoneNumber;
     this.driverCategory=driver["0"].driverCategory;
     this.employeeId=driver["0"].employeeId;
     this.driverLicenseNumber=driver["0"].driverLicenseNumber;
    });
  	this.loading = false;
  }

  updateDriverSubmit(value){

    this.loading = true;
    
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    let driver=JSON.stringify({
    							 "driverId":this.id,
								 "username":value.username,
								 "phoneNumber":value.phoneNumber,
								 "driverCategory":value.driverCategory,
								 "employeeId":value.employeeId,
								 "driverLicenseNumber":value.driverLicenseNumber 
								});
    let url:string = this.apiRoot+'hospitals/drivers/updateDriver';
    this.http.post(url,driver,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      if (res.success==true) {
        	this.loading = false;
        	this.driverList(this.id);
        	this.errmsg = "Driver Updated Succesfully.";

      }else if (res.success==false) {
        this.loading = false;
        this.errmsg = res.message;
      }else{
          this.loading = false;
         this.errmsg = "Oops ! Something went wrong..( Server Error)"; 
       }
    });
   }
}
