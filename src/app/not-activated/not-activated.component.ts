import { Component, OnInit } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-not-activated',
  templateUrl: './not-activated.component.html',
  styleUrls: ['./not-activated.component.css'],
  providers:[BnbServiceService]
})
export class NotActivatedComponent implements OnInit {

  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  public loading:boolean = false;
  apiRoot: string = this.ref.baseURL();

  ngOnInit() {
  	this.isApprovedByAdmin();
  	setInterval(()=>{this.isApprovedByAdmin(); },5 * 1000); 
  }

  isApprovedByAdmin(){
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/hospitalDetails';
    this.http.get(url,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      localStorage.setItem('approved', res.data["0"].isApprovedByAdmin);
                if (res.data["0"].isApprovedByAdmin==true) {
                        this.router.navigate(['dashboard']);
                        this.loading = false;
                      }else{
                        this.router.navigate(['notAtcivated']);
                        this.loading = false;
                      }
      //this.loading = false;
    });
  
  }
}
