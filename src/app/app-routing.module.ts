import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoolStorageModule } from 'angular2-cool-storage';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DriversComponent } from './drivers/drivers.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { AmbulanceComponent } from './ambulance/ambulance.component';
import { AddAmbulanceComponent } from './add-ambulance/add-ambulance.component';
import { ProfileComponent } from './profile/profile.component';
import { LogComponent } from './log/log.component';
import { NotActivatedComponent } from './not-activated/not-activated.component';
import {AuthGuard} from './auth.guard';
import { UpdatedriverComponent } from './updatedriver/updatedriver.component';
import { UpdateambulanceComponent } from './updateambulance/updateambulance.component';
import { TrackComponent } from './track/track.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

const routes: Routes = [
	{
	    path: '', 
	    pathMatch:'full',
	    component: DashboardComponent,
	    canActivate : [AuthGuard]
	},
	{
	    path: 'login',
	    component: LoginComponent
	},
	{
	    path: 'register',
	    component: RegisterComponent
	},
	{ path: 'dashboard',component: DashboardComponent, canActivate : [AuthGuard]},
	{ path: 'drivers',component: DriversComponent, canActivate : [AuthGuard]},
	{ path: 'addDriver',component: AddDriverComponent, canActivate : [AuthGuard]},
	{ path: 'ambulance',component: AmbulanceComponent, canActivate : [AuthGuard]},
	{ path: 'addAmbulance',component: AddAmbulanceComponent, canActivate : [AuthGuard]},
	{ path: 'profile',component: ProfileComponent, canActivate : [AuthGuard]},
	{ path: 'log',component: LogComponent, canActivate : [AuthGuard]},
	{ path: 'notAtcivated',component: NotActivatedComponent},
	{ path: 'updatedriver/:id',component: UpdatedriverComponent, canActivate : [AuthGuard]},
	{ path: 'updateambulance/:id',component: UpdateambulanceComponent, canActivate : [AuthGuard]},
	{ path: 'track/:id',component: TrackComponent, canActivate : [AuthGuard]},
	{ path: 'forgot-password',component: ForgotPasswordComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  
})
export class AppRoutingModule { }
