import { Component, OnInit, ViewChild } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-updateambulance',
  templateUrl: './updateambulance.component.html',
  styleUrls: ['./updateambulance.component.css'],
  providers:[BnbServiceService]
})
export class UpdateambulanceComponent implements OnInit {
  @ViewChild('myInput') myInputVariable: any;
  constructor(private ref:BnbServiceService, private http:Http, private router:Router,private route: ActivatedRoute) { }
  apiRoot: string = this.ref.baseURL();
  errmsg:string;
  public loading = false;
  ambulances:any[];
  ambulanceCategory:string;/*
  password:string;*/
  rcNumber:string;
  vehicleNumber:string;
  ambulanceimageUrl:string;
  public id: string;
  rcmsg:string;
  imgUrl:string='http://139.59.13.184/media/hospitals/'+localStorage.getItem('hospitalId')+'/ambulance/images/';
  public formData:FormData;
  file3:File;
  file:any;
  fileError:boolean=false;
  formFlag:boolean=true;

  ngOnInit() {
  	this.id = this.route.snapshot.paramMap.get('id');

  	this.ambulanceList(this.id);
  }
 /* fileUpload(event){
    this.formData=  new FormData();
  	this.file3 = event.target.files[0];
    console.log(this.file3);
  	this.formData.append(event.target.name, this.file3, this.file3.name);
  	console.log(this.formData);

  }*/

 fileUpload(event) {
    this.formData =  new FormData();
    this.fileError=false;
    /*this.formData.delete('file');*/
    const imageRegExt=/([a-zA-Z0-9\s_\\.\-:() ])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/;
    this.file3 = event.target.files[0];
    if (!this.file3.name.match(imageRegExt)) {
         this.formFlag=false;
         this.fileError=true;

      }
      
      this.formData.append("file", this.file3, this.file3.name);
      

      if (this.file3.name.match(imageRegExt)) {
         this.formFlag=true;
      }
     
      console.log(this.formData);
    }

  updateRc(value){
  	let status:boolean=true;
    this.loading=true;
  if (this.formData == null) {
      this.fileError=true;
      this.loading=false;
    }  
   if (this.formFlag==false) {
      this.fileError=true;
      status=false;
      this.loading=false;
    }
   if(status==true) {

  	this.formData.append("ambulanceId", this.id);
	let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);/*
    headers.append('Content-Type', 'multipart/form-data');*/
    let opts: RequestOptions = new RequestOptions();
    opts.headers = headers;
    let url = this.apiRoot+'hospitals/ambulance/updateAmbulanceImage';

	this.http.post(url, this.formData, opts)
                        .map(res => res.json())
                        .subscribe(
                            (res) => {
                              
                              if (res.success==true) {
						        	this.rcmsg = "RC Updated Succesfully.";
						        	this.ambulanceList(this.id);
						        	
						        	this.loading = false;

						      }else if (res.success==false) {
						        
						        this.rcmsg = res.message;
                    this.formData =  new FormData();
                                this.formFlag=false;
                                //this.file='';
                                console.log(this.myInputVariable.nativeElement.files);
                                this.myInputVariable.nativeElement.value = "";
                                console.log(this.myInputVariable.nativeElement.files);
                                this.file='';
                                this.fileError=false;
                                this.loading = false;
						      }else{
						          this.loading = false;
						         this.rcmsg = "Oops ! Something went wrong..( Server Error)"; 
						       }
                            })
  }else{
    
    this.rcmsg = "Oops ! Something went wrong..( Please Try Again )";
    this.loading = false;
  }
}

  ambulanceList(ambulanceId){
  	this.loading = true;
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    let ambulance;
    headers.append('Authorization', token);
    let opts = new RequestOptions();
    opts.headers = headers;
    let url:string = this.apiRoot+'hospitals/ambulance/ambulanceList';
    this.http.get(url,opts).map(response => response.json()).subscribe((ambulances)=>{
      ambulance = ambulances.data.filter(amb => amb._id == ambulanceId);
      //this.ambulances = ambulances.data;
     //console.log('driver',driver['0'].username);
    
     this.ambulanceCategory=ambulance["0"].ambulanceCategory;
     //this.password=driver["0"].password;
     this.rcNumber=ambulance["0"].rcNumber;
     this.vehicleNumber=ambulance["0"].vehicleNumber;
     this.ambulanceimageUrl=ambulance["0"].imageUrl
     this.loading = false;
    });
  	
  }


  updateambulancesubmit(value){

    this.loading = true;
    
    let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);
    headers.append('Content-Type', 'application/json');
    let opts = new RequestOptions();
    opts.headers = headers;
    let driver=JSON.stringify({
    							 "ambulanceId":this.id,
								 "ambulanceCategory":value.ambulanceCategory,
								 "rcNumber":value.rcNumber,
								 "vehicleNumber":value.vehicleNumber 
								});
    let url:string = this.apiRoot+'hospitals/ambulance/updateAmbulanceDetails';
    this.http.post(url,driver,opts).map(response => response.json()).subscribe((res)=>{
      console.log(res);
      if (res.success==true) {
        	this.errmsg = "Ambulance Updated Succesfully.";
        	this.ambulanceList(this.id);
        	
        	this.loading = false;

      }else if (res.success==false) {
        this.loading = false;
        this.errmsg = res.message;
      }else{
          this.loading = false;
         this.errmsg = "Oops ! Something went wrong..( Server Error)"; 
       }
    });
   }
}
