import { Component, OnInit,ViewChild } from '@angular/core';
import {HttpModule, Http, URLSearchParams, Headers, RequestOptions} from '@angular/http';
import{ Router, ActivatedRoute, Params } from '@angular/router';
import { BnbServiceService } from '../bnb-service.service';
import { LoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-add-ambulance',
  templateUrl: './add-ambulance.component.html',
  styleUrls: ['./add-ambulance.component.css'],
  providers:[BnbServiceService]
})
export class AddAmbulanceComponent implements OnInit {
  @ViewChild('myInput') myInputVariable: any;
  constructor(private ref:BnbServiceService, private http:Http, private router:Router) { }
  apiRoot: string = this.ref.baseURL();
  public loading = false;
  fileError:boolean=false;
  errmsg:string;
  public formData:FormData;
  file3:File;
  file:any;
  formFlag:boolean=false;
  ngOnInit() {
  }

  fileUpload(event){
    this.formData =  new FormData();
    this.fileError=false;
    /*this.formData.delete('file');*/
    const imageRegExt=/([a-zA-Z0-9\s_\\.\-:() ])+(.png|.jpg|.jpeg|.PNG|.JPG|.JPEG)$/;
    this.file3 = event.target.files[0];
    if (!this.file3.name.match(imageRegExt)) {
         this.formFlag=false;
         this.fileError=true;
      }
      
      this.formData.append("file", this.file3, this.file3.name);
      

      if (this.file3.name.match(imageRegExt)) {
         this.formFlag=true;
      }
     
      console.log(this.formData);
    }

  addAmbulance(value){
    let status:boolean=true;
    this.loading=true;
  if (this.formData == null || value.formData == "") {
      this.fileError=true;
    }	
   if (this.formFlag==false) {
      this.fileError=true;
      status=false;
    }
   if(status==true) {
	this.formData.append("vehicleNumber", value.vehicleNumber);
	this.formData.append("rcNumber", value.rcNumber);
	this.formData.append("hospitalId", localStorage.getItem('hospitalId'));
	this.formData.append("ambulanceCategory", value.ambulanceCategory);
	let token:string = localStorage.getItem('token');
    let headers: Headers = new Headers();
    headers.append('Authorization', token);/*
    headers.append('Content-Type', 'multipart/form-data');*/
    let opts: RequestOptions = new RequestOptions();
    opts.headers = headers;
    let url = this.apiRoot+'hospitals/ambulance/addambulance';
 
	this.http.post(url, this.formData, opts)
                        .map(res => res.json())
                        .subscribe(
                            (res) => {
                              if (res.success==true) {
                                  this.formData =  new FormData();
                                  this.errmsg = "Ambulance Added Succesfully.";
                                  setTimeout(function(){ location.reload() }, 2000);
                                  this.loading = false;

                              }else if (res.success==false) {
                                this.errmsg = res.message;
                                this.formData =  new FormData();
                                this.formFlag=false;
                                //this.file='';
                                console.log(this.myInputVariable.nativeElement.files);
                                this.myInputVariable.nativeElement.value = "";
                                console.log(this.myInputVariable.nativeElement.files);
                                this.file='';
                                this.fileError=false;
                                this.loading = false;
                                
                              }else{
                                  this.formData =  new FormData();
                                 this.errmsg = "Oops ! Something went wrong..( Server Error)"; 
                                  this.loading = false;
                               }
                            });
      }
  }
}
